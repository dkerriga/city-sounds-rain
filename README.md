You can find a demo [here](https://city-sounds-rain.now.sh).

The repository for the data processing and ML is on [GitHub](https://github.com/NYU-VisML-2020/CitySoundsWeather).
